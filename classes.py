class cat():
    """a simple attempt tp model a dog"""

    def __init__(self , name , age):
        """initialize name and age attributes"""
        self.name = name
        self.age = age

    def sit(self):
        """simulating a dog sitting in response to a command"""
        print(self.name.title()+" is now sitting.")
    
    def roll_over(self):
        """simulating a dog rolling over in response to a command"""
        print(self.name.title()+" rolled over!")

my_dog = cat('abraham',6)
print("my dog's name is : "+my_dog.name+" and he is "+str(my_dog.age)+" years old")
my_dog.sit()
my_dog.roll_over()

#instances as attribute
class shorthair_eye():
    
    def __init__(self,color):
        self.color = color
    
    def desc_eye(self):
        print("british eyes are "+self.color)

#inheritance
class british_shorthair(cat): #british_shorthair is child of cat
    """a class to define only british shorthair"""

    def __init__(self , name , age , color , eye):
        super().__init__(name , age)
        self.color = color
        self.eyecolor = shorthair_eye(eye) #for instance as attribute
    
    def british_color(self):
        return self.color

    #overriding methods from the parent class
    def roll_over(self):
        print("British Shorthair can not roll over")

my_british = british_shorthair('Zipper' , 6 , 'grey' , 'yellow')
my_british.sit()
my_british.roll_over()
print(my_british.name+"'s color is "+my_british.british_color())
my_british.eyecolor.desc_eye()
 

