import functions #to import whole file
functions.greeting_user()
from functions import greeting_user,build_profile #to import only selective functions
greeting_user() #with from-import no need to use modulename.functionname format
from functions import greeting_user as gu #to import function with alias name
import functions as func #to import module with ailas name
from functions import * #to import all functions in a module


