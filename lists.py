computer = ['Apple' , 'Macbook' , 'MPTT2' , '4GB GPU'] #lists use to store a series of data into one variable
print(computer[0])
print(computer[-1]) #index -1 always returns last item of the list
print(computer)
computer.append('i7 CPU') #append() method use for add extra item at the end of the list
print(computer)
computer.insert(2,'Pro') #insert() method use for add item to the list at any position
print(computer)
del computer[5] #del command use for delete item from the list in situation that programmer knows the index
print(computer)
print(computer.pop()) #pop() method drop the last item of the list and can keep that removed item in a variable
print(computer)
print(computer.pop(0)) #you can use pop() with argument to determine witch item pop
print(computer)
computer.remove("Macbook") #remove method can use for drop item that we dont know the index and we only know the value
print(computer)
#Sorting
computer = ['apple' , 'macbook' , 'mptt2' , '4GB GPU']
computer.sort() #sort() method sorts list alphabatically
print(computer)
computer.sort(reverse=True) #sort() method with this arguman sorts the list in reverse alphabetical order
print(computer)
print(sorted(computer)) #To maintain the original order of a list but present it in a sorted order, you can use the sorted() function
print(computer)
computer.reverse() #To reverse the original order of a list permanently, you can use the reverse() method.
print(computer)
print(len(computer)) #You can quickly find the length of a list by using the len() function.
"""Looping"""
for item in computer:
	print(item)
	print(item+" For 2nd time")
print("End of Loop")
#for range loop
for number in range(0,6):
	print(number)
#range() in list
rlist = list(range(0,10))
print(rlist)
even_nums = list(range(2,11,2)) #In this example, the range() function starts with the value 2 and then adds 2 to that value. It adds 2 repeatedly until it reaches or passes the end value, 11
print(even_nums)
sumth = [item+30 for item in range(1,11)] 
print(sumth)
#simple statistics 
digit = [1,4,21,5,0,-3,50,3,80]
print(max(digit))
print(min(digit))
print(sum(digit))
#slicing the list
players = ['charles', 'martina', 'michael', 'florence', 'eli']
print(players[0:3])

print(players[:4]) #Python returns all items from the beginning of the list through 5rd item

print(players[2:]) #Python returns all items from the third item through the end of the list

#copying a list
my_foods = ["Pizza" , "Spagetti" , "Falafel" , "CarrotCake"]
friends_food = my_foods[:] #without using slice the list won't copy
my_foods.append("Ice Cream")
friends_food.append("Hamburger")
print(my_foods)
print(friends_food)

#Tuples (defined lists that cant edit during program running)
tuple_list = ("laptop" , "phone" , "smartWatch") #a tuple list define by parentheses

#writing over a tuple
tuple_list = ("laptop" , "phone" , "smartWatch")
tuple_list = ("notebook" , "Mobile" , "Applewatch") #to modify a tuple you must redefine the whole tuple list






































































