"""
assertEqual(a, b)               Verify that a == b
assertNotEqual(a, b)            Verify that a != b
assertTrue(x)                   Verify that x is True
assertFalse(x)                  Verify that x is False
assertIn(item, list)            Verify that item is in list  
assertNotIn(item, list)         Verify that item is not in list
"""

print("Enter 'q' at any time to quit.")
while True:
    first = input("\nPlease give me a first name: ")
    if first == 'q':
        break
    last = input("Please give me a last name: ")
    if last == 'q':
        break
    formatted_name = get_formatted_name(first, last)
    print("\tNeatly formatted name: " + formatted_name + '.')
