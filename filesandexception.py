with open('files/pi_digits.txt') as file_object:
    contents = file_object.read()
    print(contents)

#reading line by line
with open('files/pi_digits.txt') as file_object:
    for line in file_object:
        print(line.rstrip())

#store lines of file in a list
with open('files/pi_digits.txt') as file_object:
    lines = file_object.readlines()

for line in lines:
    print(line.rstrip())

#writing into file
with open('files/test.txt' , 'w') as file_object: #w for writing r for reading a for append r+ for r/w
    file_object.write('Hello World!')

#EXCEPTIONS
try:
    print(5/0)
except ZeroDivisionError: 
    print('You cant devide by zero')

#New code for else block
#Any code that depends on the try block executing successfully goes in the else block
num1 = input('enter 2 num\nnum1:')
num2 = input('num2:')
try:
    divide = int(num1)/int(num2)
except ZeroDivisionError:
    print("You Can't Divide by Zero")
else:
    print(divide)

#Failing Silently
try:
    print(5/0)
except ZeroDivisionError:
    pass

#USE JSON MODULE TO STORE DATA
import json
numbers = [1,2,3,4,5,6,7,8,9]
filename = 'files/numbers.json'
with open(filename , 'w') as file_object:
    json.dump(numbers , file_object)
#read from json module
with open(filename , 'r') as file_object:
    json.load(file_object)
    print(numbers)

