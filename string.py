name = "MY NAME is john BRADLEY"
print(name.title()) #title() displays each word in titlecase, where each word begins with a capital letter.
print(name.upper()) #Converts all characters to upper case
print(name.lower()) #Converts all characters to lower case
#Combining Strings
firstname = 'John'
lastname = 'Bradley'
message = "My Name is "+firstname+" "+lastname
print(message)
#Whitespaces
print("\tits tab whitespaces") #\t can make tab whitespaces
print("Languages:\nPython\nC\nJavaScript") #\n can make newline
message = "  Striping Method  "
#Stripping Tools
print(message.rstrip()) #rstrip method fixs extra space at the end of string
print(message.lstrip()) #lstrip method fixs extra space at the beginning of string
print(message.strip()) #rstrip method fixs extra space at both end and beginnig of string
print ("hello")

#split() Method
content = "Alice in Wonderland"
split = content.split()
print(split)
