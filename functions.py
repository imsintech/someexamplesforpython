def greeting_user():
    #Display a single greeting
    message = "Hello new User!"
    print(message)
    return message
print(greeting_user()+" in return")
#send a copy of list to function
def poplist(list):
    while list:
        print(list.pop())
lis = ['john' , 'ty' , 'tyrion']
poplist(lis[:])
print(lis)

#passing an arbitary number of arguments
def desirefoods(*foods): #asterisk(*) sign build a tuple
    print(foods)
desirefoods('pizza','hamburger','lazania')
desirefoods('ghormesabzi','gheyme')

#passing arbitary keyword args
def build_profile(first , last , **info):
    print(first+" "+last)
    for key,val in info.items():
        print(key+" : "+val)
build_profile('John' , 'Snow' , location = 'westeros' , sword = 'longclow' , wife = 'roze')
