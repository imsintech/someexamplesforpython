dictionary01 = {'firstname' :"John" , 'lastname' : "Snow"} #dictionaryName = {key : value}
print(dictionary01)

#adding new key-value to dictionary
dictionary01['username'] = 'john433'
dictionary01['password'] = '123'
print(dictionary01)

#delete a piece of information
del dictionary01['password']
print(dictionary01)

#looping throgh all key-value pairs
for key,value in dictionary01.items():
	print("\nKey = "+key)
	print("Value = "+value)
print(dictionary01.items())

#keys() Method for looping throgh keys
for key in dictionary01.keys():
	print(key)
#or you can use:
print("////////////")
for key in dictionary01:
	print(key)

#retrive dictionary data sorted
print("////////////")
for key,value in sorted(dictionary01.items()):
	print(key)
	print(" "+value)

#values() Method for looping throgh values
print("////////////")
for value in dictionary01.values():
	print(value)
#set() function use when we need to remove same values
print("////////////")
repetetive_dic = {"name" : "John" , "lname" : "John" , "username" : "John"}
for value in set(repetetive_dic.values()):
	print(value)

#Nesting
#A List of Dictionaries
#Dictionaris
user01 = {'name':'john' , 'lastname':'snow'}
user02 = {'name':'arya' , 'lastname':'stark'}
user03 = {'name':'denerys' , 'lastname':'targerian'}
#list of dictionaris
users = [user01 , user02 , user03]
print(users[1])

#A list inside Dictionary
dic = {
	'name' : 'sara' ,
	'interestings' : ['computer' , 'programming' , 'network']
}
print(dic['name']+"'s interestings is :")
for interest in dic['interestings']:
	print("\t"+interest)

#A dictionary inisde dictionary
users = {
	'user01' : {'name':'john' , 'lastname':'snow'},
	'user02' : {'name':'arya' , 'lastname':'stark'},
	'user03' : {'name':'denerys' , 'lastname':'targerian'}
}
print(users['user02'])

#Add data to dictionary
dic['lastname'] = 'tenta'
print(dic)
























