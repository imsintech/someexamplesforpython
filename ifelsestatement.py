#if,else statement
cars = [ "lamborghini" , "porche" , "bmw" , "ferari"]
for car in cars:
	if car == "bmw":
		print(car.upper())
	else:
		print(car)

#cheking wether a value is in a list or not
toppings = ["mushrooms" , "LSD" , "Weed"]
print("mushrooms" in toppings)
print("Crock" not in toppings)
print("mushrooms" not in toppings)
print("Crock" not in toppings)

#if-elif-else
age = 14
if age <= 4 :
	print("age <= 4 ")
elif age <= 10 :
	print("age <= 10 ")
elif age <= 15 :
	print("age <= 15 ")
elif age <= 20 :
	print("age <= 20 ")
else :
	print("age > 20 ")
	
#checking that a list is not empty
requested_items = []

if requested_items:
	print("list is not empty")
else:
	print("list is empty")

